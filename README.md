# Liquid Disqus Tag [![Gem Version](https://badge.fury.io/rb/liquid-disqus.svg)](https://badge.fury.io/rb/liquid-disqus)

This plugin helps generate Disqus code snippet easily as a Liquid Tag call

### Installation

This plugin is available as a [RubyGem](https://rubygems.org/gems/liquid-disqus/)

Add this line to your application's Gemfile:

```
gem 'liquid-disqus'
```

And then execute the `bundle install` command to install the gem.

Alternatively, you can also manually install the gem using the following command:

```
$ gem install liquid-disqus
```

For Jekyll Users, after the plugin has been installed successfully, add the following lines to your _config.yml in order to tell Jekyll to use the plugin:

```yaml
gems:
  - liquid-disqus
```

### Usage

You need to use `disqus` as liquid tag and pass the Tracking Code as parameter shown below

```
{{ disqus saiprojectideas }}
```

### Contribute

Fork this repository, make your changes and then issue a pull request. If you find bugs or have new ideas that you do not want to implement yourself, file a bug report.

### Copyright

Copyright (c) 2016 Rohan Sakhale

License: MIT